CREATE DATABASE pimcore CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'pimcore'@'localhost' IDENTIFIED BY 'pimcore';
GRANT ALL PRIVILEGES ON pimcore.* TO 'pimcore'@'localhost';
