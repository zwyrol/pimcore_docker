composer create-project pimcore/skeleton output
mv output/* .
rm -rf output
mysql -u root < docker/container_data/dump.sql

./vendor/bin/pimcore-install --admin-username=admin --admin-password=admin --mysql-username=pimcore --mysql-password=pimcore --mysql-database=pimcore

